@extends('_layouts.master')

@section('content')
    <div class="content">
        <div class="m-b-md">
            <h1 class="title">Daria Mokrytska</h1>

            <p>personal webpage</p>
        </div>

        <div align="center">           
            
          <table>
            <tr>
              <td>
                <a href="/about">About</a>
                
              </td>
              <td>
                
              </td>
              <td>
                <a href="mailto:mokrytska.d@gmail.com">Contact</a>
              </td>
            </tr>

            <tr>
              <td>
                <a href="/files/mokrytska_cv.pdf" download="mokrytska_cv.pdf">CV</a>
                
              </td>
              <td>
                
              </td>
              <td>
                <a href="/art">Art</a>
              </td>
            </tr>
            
            
            
          </table>
            
        </div>
    </div>
@endsection
